require('dotenv').config()
const http = require('http')
const path = require('path')
const fs = require('fs-extra')
const { Nuxt, Builder } = require('nuxt')
const express = require('express')
const app = express()
const server = http.createServer(app)
const cors = require('cors')
const io = require('socket.io')(server)
const redis = require('redis')
const session = require('express-session')
const RedisStore = require('connect-redis')(session)
let db
if (process.env.NODE_ENV === 'production') {
	db = redis.createClient({ host: process.env.DB_HOST, port: 6379, password: process.env.DB_PWD })
} else {
	db = redis.createClient()
}
const bodyParser = require('body-parser')
const helmet = require('helmet')
const v = require('voca')
const multer = require('multer')
const PDFDocument = require('pdfkit')
const sharp = require('sharp')
const moment = require('moment')

let storeOptions
if (process.env.NODE_ENV === 'production') {
	storeOptions = {
		host: process.env.DB_HOST,
		port: 6379,
		pass: process.env.DB_PWD,
		client: db,
		prefix: 'sessions:'
	}
} else {
	storeOptions = {
		host: 'localhost',
		port: 6379,
		client: db,
		prefix: 'sessions:'
	}
}
const sessionOptions = {
	secret: process.env.SESSION_KEY,
	store: new RedisStore(storeOptions),
	name: 'digistorm',
	resave: false,
	rolling: true,
	saveUninitialized: false
}
const expressSession = session(sessionOptions)
const sharedsession = require('express-socket.io-session')
const config = require('../nuxt.config.js')

config.dev = !(process.env.NODE_ENV === 'production')
const nuxt = new Nuxt(config)
const { host, port } = nuxt.options.server

if (config.dev) {
	process.env.DEBUG = 'nuxt:*'
	const builder = new Builder(nuxt)
	builder.build()
} else {
	nuxt.ready()
}

const t = require(path.join(__dirname, '..', '/lang/lang.js'))

app.set('trust proxy', true)
app.use(helmet())
app.use(bodyParser.json({ limit: '10mb' }))
app.use(expressSession)
io.use(sharedsession(expressSession, {
	autoSave: true
}))
app.use(cors())

app.get('/c/:code', function (req, res) {
	if (req.session.interactions && req.session.interactions.length > 0) {
		const code = parseInt(req.params.code)
		const admin = req.session.interactions.map(item => item.code).includes(code)
		if (admin === true) {
			req.next()
		} else {
			res.redirect('/')
		}
	} else {
		res.redirect('/')
	}
})

app.get('/p/:code', function (req) {
	if (req.session.identifiant === '' || req.session.identifiant === undefined) {
		const identifiant = 'u' + Math.random().toString(16).slice(3)
		req.session.identifiant = identifiant
		req.session.nom = ''
		req.session.langue = 'fr'
		req.session.interactions = []
		req.session.cookie.expires = new Date(Date.now() + (3600 * 24 * 7 * 1000))
	}
	req.next()
})

app.post('/api/rejoindre-interaction', function (req, res) {
	const code = parseInt(req.body.code)
	db.exists('interactions:' + code, function (err, reponse) {
		if (err) { res.send('erreur') }
		if (reponse === 1) {
			if (req.session.identifiant === '' || req.session.identifiant === undefined) {
				const identifiant = 'u' + Math.random().toString(16).slice(3)
				req.session.identifiant = identifiant
				req.session.nom = ''
				req.session.langue = 'fr'
				req.session.interactions = []
				req.session.cookie.expires = new Date(Date.now() + (3600 * 24 * 7 * 1000))
			}
			res.json({ code: code, identifiant: req.session.identifiant })
		} else {
			res.send('erreur_code')
		}
	})
})

app.post('/api/creer-interaction', function (req, res) {
	if (req.session.identifiant === '' || req.session.identifiant === undefined) {
		const identifiant = 'u' + Math.random().toString(16).slice(3)
		req.session.identifiant = identifiant
		req.session.interactions = []
	}
	const titre = req.body.titre
	const type = req.body.type
	const code = Math.floor(100000 + Math.random() * 900000)
	const motdepasse = creerMotDePasse()
	const date = moment().format()
	db.exists('interactions:' + code, function (err, reponse) {
		if (err) { res.send('erreur') }
		if (reponse === 0) {
			db.hmset('interactions:' + code, 'type', type, 'titre', titre, 'code', code, 'motdepasse', motdepasse, 'donnees', JSON.stringify({}), 'reponses', JSON.stringify({}), 'sessions', JSON.stringify({}), 'statut', '', 'session', 1, 'date', date, function (err) {
				if (err) { res.send('erreur') }
				const chemin = path.join(__dirname, '..', '/static/fichiers/' + code)
				fs.mkdirsSync(chemin)
				req.session.nom = 'admin'
				req.session.langue = 'fr'
				req.session.interactions.push({ code: code, motdepasse: motdepasse })
				req.session.cookie.expires = new Date(Date.now() + (3600 * 24 * 7 * 1000))
				res.json({ code: code, identifiant: req.session.identifiant })
			})
		} else {
			res.send('existe_deja')
		}
	})
})

app.post('/api/modifier-interaction', function (req, res) {
	const identifiant = req.body.identifiant
	if (req.session.identifiant && req.session.identifiant === identifiant) {
		const code = parseInt(req.body.code)
		db.exists('interactions:' + code, function (err, reponse) {
			if (err) { res.send('erreur') }
			if (reponse === 1) {
				db.hgetall('interactions:' + code, function (err, resultat) {
					if (err) { res.send('erreur') }
					const titre = req.body.titre
					const type = resultat.type
					const donnees = req.body.donnees
					const donneesActuelles = JSON.parse(resultat.donnees)
					const fichiersActuels = []
					const fichiers = []
					const corbeille = []
					if (Object.keys(donneesActuelles).length > 0) {
						if (Object.keys(donneesActuelles.support).length > 0) {
							if (type === 'Questionnaire') {
								fichiersActuels.push(donneesActuelles.support.fichier)
							} else {
								fichiersActuels.push(donneesActuelles.support.image)
							}
						}
						if (type === 'Sondage') {
							donneesActuelles.items.forEach(function (item) {
								if (item.image !== '') {
									fichiersActuels.push(item.image)
								}
							})
						} else if (type === 'Remue-méninges') {
							donneesActuelles.categories.forEach(function (categorie) {
								if (categorie.image !== '') {
									fichiersActuels.push(categorie.image)
								}
							})
						} else if (type === 'Questionnaire') {
							donneesActuelles.questions.forEach(function (q) {
								q.items.forEach(function (item) {
									if (item.image !== '') {
										fichiersActuels.push(item.image)
									}
								})
							})
						}
						if (Object.keys(donnees.support).length > 0) {
							if (type === 'Questionnaire') {
								fichiers.push(donnees.support.fichier)
							} else {
								fichiers.push(donnees.support.image)
							}
						}
						if (type === 'Sondage') {
							donnees.items.forEach(function (item) {
								if (item.image !== '') {
									fichiers.push(item.image)
								}
							})
						} else if (type === 'Remue-méninges') {
							donnees.categories.forEach(function (categorie) {
								if (categorie.image !== '') {
									fichiers.push(categorie.image)
								}
							})
						} else if (type === 'Questionnaire') {
							donnees.questions.forEach(function (q) {
								q.items.forEach(function (item) {
									if (item.image !== '') {
										fichiers.push(item.image)
									}
								})
							})
						}
						fichiersActuels.forEach(function (fichier) {
							if (!fichiers.includes(fichier)) {
								corbeille.push(fichier)
							}
						})
					}
					db.hmset('interactions:' + code, 'titre', titre, 'donnees', JSON.stringify(donnees), function (err) {
						if (err) { res.send('erreur') }
						if (corbeille.length > 0) {
							corbeille.forEach(function (fichier) {
								supprimerFichier(code, fichier)
							})
						}
						res.send('donnees_enregistrees')
					})
				})
			} else {
				res.send('erreur_code')
			}
		})
	} else {
		res.send('non_autorise')
	}
})

app.post('/api/modifier-statut-interaction', function (req, res) {
	const identifiant = req.body.identifiant
	if (req.session.identifiant && req.session.identifiant === identifiant) {
		const code = parseInt(req.body.code)
		db.exists('interactions:' + code, function (err, reponse) {
			if (err) { res.send('erreur') }
			if (reponse === 1) {
				const statut = req.body.statut
				if (statut === 'ouvert') {
					db.hgetall('interactions:' + code, function (err, resultat) {
						if (err) { res.send('erreur') }
						const date = moment().format()
						const session = resultat.session
						const sessions = JSON.parse(resultat.sessions)
						sessions[session] = {}
						sessions[session].debut = date
						db.hmset('interactions:' + code, 'statut', statut, 'sessions', JSON.stringify(sessions), function (err) {
							if (err) { res.send('erreur') }
							res.send('statut_modifie')
						})
					})
				} else {
					db.hmset('interactions:' + code, 'statut', statut, function (err) {
						if (err) { res.send('erreur') }
						res.send('statut_modifie')
					})
				}
			} else {
				res.send('erreur_code')
			}
		})
	} else {
		res.send('non_autorise')
	}
})

app.post('/api/modifier-index-question', function (req, res) {
	const identifiant = req.body.identifiant
	if (req.session.identifiant && req.session.identifiant === identifiant) {
		const code = parseInt(req.body.code)
		const indexQuestion = req.body.indexQuestion
		db.exists('interactions:' + code, function (err, reponse) {
			if (err) { res.send('erreur') }
			if (reponse === 1) {
				db.hgetall('interactions:' + code, function (err, resultat) {
					if (err) { res.send('erreur') }
					const donnees = JSON.parse(resultat.donnees)
					donnees.indexQuestion = indexQuestion
					db.hmset('interactions:' + code, 'donnees', JSON.stringify(donnees), function (err) {
						if (err) { res.send('erreur') }
						res.send('index_modifie')
					})
				})
			} else {
				res.send('erreur_code')
			}
		})
	} else {
		res.send('non_autorise')
	}
})

app.post('/api/fermer-interaction', function (req, res) {
	const identifiant = req.body.identifiant
	if (req.session.identifiant && req.session.identifiant === identifiant) {
		const code = parseInt(req.body.code)
		db.exists('interactions:' + code, function (err, reponse) {
			if (err) { res.send('erreur') }
			if (reponse === 1) {
				db.hgetall('interactions:' + code, function (err, resultat) {
					if (err) { res.send('erreur') }
					const date = moment().format()
					let session = resultat.session
					const type = resultat.type
					const donnees = JSON.parse(resultat.donnees)
					const reponses = JSON.parse(resultat.reponses)
					const sessions = JSON.parse(resultat.sessions)
					if (reponses[session] && reponses[session].length > 0) {
						sessions[session].fin = date
						sessions[session].donnees = donnees
						if (type === 'Questionnaire') {
							sessions[session].classement = req.body.classement
						}
					} else {
						delete sessions[session]
					}
					session = parseInt(session) + 1
					if (type === 'Questionnaire') {
						donnees.indexQuestion = donnees.copieIndexQuestion
						db.hmset('interactions:' + code, 'statut', 'termine', 'donnees', JSON.stringify(donnees), 'sessions', JSON.stringify(sessions), 'session', session, function (err) {
							if (err) { res.send('erreur') }
							res.json({ session: session, reponses: reponses, sessions: sessions })
						})
					} else {
						db.hmset('interactions:' + code, 'statut', 'termine', 'sessions', JSON.stringify(sessions), 'session', session, function (err) {
							if (err) { res.send('erreur') }
							res.json({ session: session, reponses: reponses, sessions: sessions })
						})
					}
				})
			} else {
				res.send('erreur_code')
			}
		})
	} else {
		res.send('non_autorise')
	}
})

app.post('/api/se-connecter-interaction', function (req, res) {
	if (req.session.identifiant === '' || req.session.identifiant === undefined) {
		const identifiant = 'u' + Math.random().toString(16).slice(3)
		req.session.identifiant = identifiant
		req.session.interactions = []
	}
	const code = parseInt(req.body.code)
	const motdepasse = req.body.motdepasse
	db.exists('interactions:' + code, function (err, reponse) {
		if (err) { res.send('erreur') }
		if (reponse === 1) {
			db.hgetall('interactions:' + code, function (err, resultat) {
				if (err) { res.send('erreur') }
				if (motdepasse !== '' && motdepasse === resultat.motdepasse) {
					req.session.nom = 'admin'
					req.session.langue = 'fr'
					req.session.cookie.expires = new Date(Date.now() + (3600 * 24 * 7 * 1000))
					req.session.interactions.push({ code: code, motdepasse: motdepasse })
					res.json({ code: code, identifiant: req.session.identifiant })
				} else {
					res.send('non_autorise')
				}
			})
		} else {
			res.send('erreur_code')
		}
	})
})

app.post('/api/recuperer-donnees-interaction', function (req, res) {
	const code = parseInt(req.body.code)
	db.exists('interactions:' + code, function (err, reponse) {
		if (err) { res.send('erreur') }
		if (reponse === 1) {
			db.hgetall('interactions:' + code, function (err, resultat) {
				if (err) { res.send('erreur') }
				const type = resultat.type
				const titre = resultat.titre
				const motdepasse = resultat.motdepasse
				const donnees = JSON.parse(resultat.donnees)
				const reponses = JSON.parse(resultat.reponses)
				const sessions = JSON.parse(resultat.sessions)
				const statut = resultat.statut
				const session = parseInt(resultat.session)
				res.json({ type: type, titre: titre, motdepasse: motdepasse, donnees: donnees, reponses: reponses, sessions: sessions, statut: statut, session: session })
			})
		} else {
			res.send('erreur')
		}
	})
})

app.post('/api/telecharger-informations-interaction', function (req, res) {
	const identifiant = req.body.identifiant
	if (req.session.identifiant && req.session.identifiant === identifiant) {
		const code = parseInt(req.body.code)
		const motdepasse = req.body.motdepasse
		const type = req.body.type
		const titre = req.body.titre
		const doc = new PDFDocument()
		const fichier = code + '_' + Math.random().toString(36).substring(2, 12) + '.pdf'
		const chemin = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + fichier)
		const flux = fs.createWriteStream(chemin)
		doc.pipe(flux)
		doc.fontSize(18)
		if (type === 'Sondage') {
			doc.font('Helvetica-Bold').text(t[req.session.langue].sondage + ' - ' + titre)
		} else if (type === 'Remue-méninges') {
			doc.font('Helvetica-Bold').text(t[req.session.langue].remueMeninges + ' - ' + titre)
		} else if (type === 'Questionnaire') {
			doc.font('Helvetica-Bold').text(t[req.session.langue].questionnaire + ' - ' + titre)
		}
		doc.moveDown()
		doc.fontSize(14)
		doc.font('Helvetica-Bold').text(t[req.session.langue].code + ' ' + code)
		doc.moveDown()
		doc.font('Helvetica-Bold').text(t[req.session.langue].lien + ' https://digistorm.app/p/' + code)
		doc.moveDown()
		doc.font('Helvetica-Bold').text(t[req.session.langue].motdepasse + ' ' + motdepasse)
		doc.moveDown()
		doc.end()
		flux.on('finish', function () {
			res.send(fichier)
		})
	} else {
		res.send('non_autorise')
	}
})

app.post('/api/supprimer-informations-interaction', function (req, res) {
	const code = parseInt(req.body.code)
	const fichier = req.body.fichier
	supprimerFichier(code, fichier)
	res.send('fichier_supprime')
})

app.post('/api/exporter-resultat', function (req, res) {
	const identifiant = req.body.identifiant
	if (req.session.identifiant && req.session.identifiant === identifiant) {
		const code = parseInt(req.body.code)
		const type = req.body.type
		const titre = req.body.titre
		const donnees = req.body.donnees
		const reponses = req.body.reponses
		const date = req.body.date
		const alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
		const doc = new PDFDocument()
		const chemin = path.join(__dirname, '..', '/static/fichiers/' + code + '/resultats.pdf')
		const flux = fs.createWriteStream(chemin)
		doc.pipe(flux)
		doc.fontSize(16)
		if (type === 'Sondage') {
			doc.font('Helvetica-Bold').text(t[req.session.langue].sondage + ' - ' + titre)
		} else if (type === 'Remue-méninges') {
			doc.font('Helvetica-Bold').text(t[req.session.langue].remueMeninges + ' - ' + titre)
		} else if (type === 'Questionnaire') {
			doc.font('Helvetica-Bold').text(t[req.session.langue].questionnaire + ' - ' + titre)
		}
		doc.fontSize(10)
		doc.moveDown()
		if (type === 'Sondage') {
			doc.fontSize(8)
			doc.font('Helvetica').text(formaterDate(date, t[req.session.langue].resultats, req.session.langue))
			doc.moveDown()
			doc.moveDown()
			doc.fontSize(12)
			doc.font('Helvetica-Bold').text(t[req.session.langue].question, { underline: true })
			if (donnees.question !== '') {
				doc.moveDown()
				doc.font('Helvetica-Bold').text(donnees.question)
			}
			if (Object.keys(donnees.support).length > 0) {
				doc.moveDown()
				const cheminSupport = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + donnees.support.image)
				if (fs.existsSync(cheminSupport)) {
					const support = fs.readFileSync(cheminSupport)
					doc.image(support, { fit: [120, 120] })
				} else {
					doc.fontSize(10)
					doc.font('Helvetica').text(t[req.session.langue].image + ' ' + donnees.support.alt)
				}
			}
			doc.moveDown()
			doc.fontSize(12)
			doc.font('Helvetica-Bold').text(t[req.session.langue].reponses + ' (' + reponses.length + ')', { underline: true })
			doc.moveDown()
			const items = donnees.items
			const statistiques = definirStatistiquesSondage(items, reponses)
			items.forEach(function (item, index) {
				if (item.texte !== '') {
					doc.fontSize(10)
					doc.font('Helvetica').text(alphabet[index] + '. ' + item.texte + ' (' + statistiques.pourcentages[index] + '% - ' + statistiques.personnes[index] + ')')
					if (item.image !== '') {
						const cheminImage = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + item.image)
						if (fs.existsSync(cheminImage)) {
							const image = fs.readFileSync(cheminImage)
							doc.image(image, { fit: [75, 75] })
						}
					}
				} else if (item.image !== '') {
					doc.fontSize(10)
					const cheminImage = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + item.image)
					if (fs.existsSync(cheminImage)) {
						const image = fs.readFileSync(cheminImage)
						doc.font('Helvetica').text(alphabet[index] + '. (' + statistiques.pourcentages[index] + '% - ' + statistiques.personnes[index] + ')').image(image, { fit: [75, 75] })
					} else {
						doc.font('Helvetica').text(alphabet[index] + '. ' + item.alt + ' (' + statistiques.pourcentages[index] + '% - ' + statistiques.personnes[index] + ')')
					}
				}
				doc.moveDown()
			})
		} else if (type === 'Remue-méninges') {
			doc.fontSize(8)
			doc.font('Helvetica').text(formaterDate(date.debut, t[req.session.langue].resultats, req.session.langue))
			doc.moveDown()
			doc.moveDown()
			doc.fontSize(12)
			doc.font('Helvetica-Bold').text(t[req.session.langue].question, { underline: true })
			doc.moveDown()
			doc.font('Helvetica-Bold').text(donnees.question)
			if (Object.keys(donnees.support).length > 0) {
				doc.moveDown()
				const cheminSupport = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + donnees.support.image)
				if (fs.existsSync(support)) {
					const support = fs.readFileSync(cheminSupport)
					doc.image(support, { fit: [120, 120] })
				}
			}
			doc.moveDown()
			doc.fontSize(12)
			doc.font('Helvetica-Bold').text(t[req.session.langue].reponses + ' (' + reponses.length + ')', { underline: true })
			doc.moveDown()
			const categories = donnees.categories.filter(function (categorie) {
				return categorie.texte !== '' || categorie.image !== ''
			})
			const messages = definirMessagesRemueMeninges(categories, reponses)
			if (categories.length > 0) {
				categories.forEach(function (categorie, index) {
					if (categorie.texte !== '') {
						doc.fontSize(10)
						doc.font('Helvetica-Bold').text(alphabet[index] + '. ' + categorie.texte + ' (' + messages.visibles[index].length + ')')
						if (categorie.image !== '') {
							const cheminImage = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + categorie.image)
							if (fs.existsSync(cheminImage)) {
								const image = fs.readFileSync(cheminImage)
								doc.image(image, { fit: [40, 40] })
								doc.moveDown()
							}
						}
						messages.visibles[index].forEach(function (message) {
							doc.fontSize(9)
							doc.font('Helvetica').text('• ' + message.reponse.texte)
							doc.moveDown()
						})
					} else if (categorie.image !== '') {
						doc.fontSize(10)
						const cheminImage = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + categorie.image)
						if (fs.existsSync(cheminImage)) {
							const image = fs.readFileSync(cheminImage)
							doc.font('Helvetica-Bold').text(alphabet[index] + '. (' + messages.visibles[index].length + ')').image(image, { fit: [40, 40] })
							doc.moveDown()
						} else {
							doc.font('Helvetica-Bold').text(alphabet[index] + '. ' + categorie.alt + ' (' + messages.visibles[index].length + ')')
							doc.moveDown()
						}
						messages.visibles[index].forEach(function (message) {
							doc.fontSize(9)
							doc.font('Helvetica').text('• ' + message.reponse.texte)
							doc.moveDown()
						})
					}
					doc.moveDown()
				})
			} else {
				messages.visibles.forEach(function (message) {
					doc.fontSize(9)
					doc.font('Helvetica').text('• ' + message.reponse.texte)
					doc.moveDown()
				})
			}
		} else if (type === 'Questionnaire') {
			const statistiques = definirStatistiquesQuestionnaire(donnees.questions, reponses)
			const classement = req.body.classement
			doc.fontSize(8)
			doc.font('Helvetica').text(formaterDate(date, t[req.session.langue].resultats, req.session.langue))
			doc.moveDown()
			if (donnees.options.progression === 'libre') {
				doc.font('Helvetica').text(t[req.session.langue].progression + ' ' + t[req.session.langue].progressionLibre)
			} else {
				doc.font('Helvetica').text(t[req.session.langue].progression + ' ' + t[req.session.langue].progressionAnimateur)
			}
			doc.moveDown()
			doc.moveDown()
			if (donnees.description !== '') {
				doc.fontSize(12)
				doc.font('Helvetica-Bold').text(t[req.session.langue].description)
				doc.fontSize(10)
				doc.moveDown()
				doc.font('Helvetica').text(donnees.description)
			}
			if (donnees.description !== '' && Object.keys(donnees.support).length > 0) {
				doc.fontSize(12)
				doc.moveDown()
			}
			if (Object.keys(donnees.support).length > 0) {
				doc.fontSize(12)
				doc.font('Helvetica-Bold').text(t[req.session.langue].support)
				doc.fontSize(10)
				doc.moveDown()
				if (donnees.support.type === 'image') {
					const cheminSupport = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + donnees.support.fichier)
					if (fs.existsSync(cheminSupport)) {
						const support = fs.readFileSync(cheminSupport)
						doc.image(support, { fit: [120, 120] })
					} else {
						doc.font('Helvetica').text(t[req.session.langue].image + ' ' + donnees.support.alt)
					}
				} else if (donnees.support.type === 'audio') {
					doc.font('Helvetica').text(t[req.session.langue].fichierAudio + ' ' + donnees.support.alt)
				} else if (donnees.support.type === 'video') {
					doc.font('Helvetica').text(t[req.session.langue].video + ' ' + donnees.support.lien)
				}
			}
			if (donnees.description !== '' || Object.keys(donnees.support).length > 0) {
				doc.fontSize(12)
				doc.moveDown()
				doc.moveDown()
			}
			donnees.questions.forEach(function (q, indexQ) {
				doc.fontSize(14)
				doc.font('Helvetica-Bold').fillColor('black').text(t[req.session.langue].question + ' ' + (indexQ + 1))
				doc.fontSize(10)
				doc.font('Helvetica').text('-----------------------------------------------')
				doc.fontSize(14)
				doc.moveDown()
				doc.fontSize(12)
				doc.font('Helvetica-Bold').text(t[req.session.langue].question)
				if (q.question !== '') {
					doc.moveDown()
					doc.font('Helvetica-Bold').text(q.question)
				}
				if (Object.keys(q.support).length > 0) {
					doc.moveDown()
					const cheminSupport = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + q.support.image)
					if (fs.existsSync(cheminSupport)) {
						const support = fs.readFileSync(cheminSupport)
						doc.image(support, { fit: [120, 120] })
					}
				}
				doc.moveDown()
				doc.moveDown()
				doc.fontSize(12)
				doc.font('Helvetica-Bold').text(t[req.session.langue].reponses + ' (' + reponses.length + ')')
				doc.moveDown()
				q.items.forEach(function (item, index) {
					if (item.texte !== '') {
						doc.fontSize(10)
						if (item.reponse === true) {
							doc.font('Helvetica').fillColor('#00a695').text(alphabet[index] + '. ' + item.texte + ' (' + statistiques[indexQ].pourcentages[index] + '% - ' + statistiques[indexQ].personnes[index] + ') - ' + t[req.session.langue].bonneReponse)
						} else {
							doc.font('Helvetica').fillColor('grey').text(alphabet[index] + '. ' + item.texte + ' (' + statistiques[indexQ].pourcentages[index] + '% - ' + statistiques[indexQ].personnes[index] + ')')
						}
						if (item.image !== '') {
							const cheminImage = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + item.image)
							if (fs.existsSync(cheminImage)) {
								const image = fs.readFileSync(cheminImage)
								doc.image(image, { fit: [75, 75] })
							}
						}
					} else if (item.image !== '') {
						doc.fontSize(10)
						const cheminImage = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + item.image)
						if (fs.existsSync(cheminImage)) {
							const image = fs.readFileSync(cheminImage)
							if (item.reponse === true) {
								doc.font('Helvetica').fillColor('#00a695').text(alphabet[index] + '. (' + statistiques[indexQ].pourcentages[index] + '% - ' + statistiques[indexQ].personnes[index] + ') - ' + t[req.session.langue].bonneReponse).image(image, { fit: [75, 75] })
							} else {
								doc.font('Helvetica').fillColor('grey').text(alphabet[index] + '. (' + statistiques[indexQ].pourcentages[index] + '% - ' + statistiques[indexQ].personnes[index] + ')').image(image, { fit: [75, 75] })
							}
						} else {
							if (item.reponse === true) {
								doc.font('Helvetica').fillColor('#00a695').text(alphabet[index] + '. ' + item.alt + ' (' + statistiques[indexQ].pourcentages[index] + '% - ' + statistiques[indexQ].personnes[index] + ') - ' + t[req.session.langue].bonneReponse)
							} else {
								doc.font('Helvetica').fillColor('grey').text(alphabet[index] + '. ' + item.alt + ' (' + statistiques[indexQ].pourcentages[index] + '% - ' + statistiques[indexQ].personnes[index] + ')')
							}
						}
					}
					doc.moveDown()
				})
				doc.moveDown()
				doc.moveDown()
			})
			if (classement.length > 0 && donnees.options.nom === 'obligatoire') {
				doc.fontSize(14)
				doc.font('Helvetica-Bold').fillColor('black').text(t[req.session.langue].classement)
				doc.fontSize(10)
				doc.font('Helvetica').text('-----------------------------------------------')
				doc.fontSize(14)
				doc.moveDown()
				doc.fontSize(12)
				classement.forEach(function (utilisateur, indexUtilisateur) {
					doc.font('Helvetica').text((indexUtilisateur + 1) + '. ' + utilisateur.nom + ' (' + utilisateur.score + ' ' + t[req.session.langue].points + ')')
					doc.moveDown()
				})
			}
		}
		doc.end()
		flux.on('finish', function () {
			res.send('resultat_exporte')
		})
	} else {
		res.send('non_autorise')
	}
})

app.post('/api/supprimer-resultat', function (req, res) {
	const identifiant = req.body.identifiant
	if (req.session.identifiant && req.session.identifiant === identifiant) {
		const code = parseInt(req.body.code)
		const session = parseInt(req.body.session)
		db.exists('interactions:' + code, function (err, reponse) {
			if (err) { res.send('erreur') }
			if (reponse === 1) {
				db.hgetall('interactions:' + code, function (err, donnees) {
					if (err) { res.send('erreur') }
					const reponses = JSON.parse(donnees.reponses)
					const sessions = JSON.parse(donnees.sessions)
					if (reponses[session]) {
						delete reponses[session]
					}
					if (sessions[session]) {
						delete sessions[session]
					}
					db.hmset('interactions:' + code, 'reponses', JSON.stringify(reponses), 'sessions', JSON.stringify(sessions), function (err) {
						if (err) { res.send('erreur') }
						res.json({ reponses: reponses, sessions: sessions })
					})
				})
			} else {
				res.send('erreur_code')
			}
		})
	} else {
		res.send('non_autorise')
	}
})

app.post('/api/televerser-image', function (req, res) {
	const identifiant = req.session.identifiant
	if (!identifiant) {
		res.send('non_autorise')
	} else {
		televerser(req, res, function () {
			const fichier = req.file
			const alt = path.parse(fichier.originalname).name
			const code = req.body.code
			const chemin = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + fichier.filename)
			const extension = path.parse(fichier.filename).ext
			if (extension.toLowerCase() === '.jpg' || extension.toLowerCase() === '.jpeg') {
				sharp(chemin).withMetadata().rotate().jpeg().resize(1000, 1000, {
					kernel: sharp.kernel.nearest,
					fit: 'inside'
				}).toBuffer((err, buffer) => {
					if (err) { res.send('erreur') }
					fs.writeFile(chemin, buffer, function() {
						res.json({ image: fichier.filename, alt: alt })
					})
				})
			} else {
				sharp(chemin).withMetadata().resize(1000, 1000, {
					kernel: sharp.kernel.nearest,
					fit: 'inside'
				}).toBuffer((err, buffer) => {
					if (err) { res.send('erreur') }
					fs.writeFile(chemin, buffer, function() {
						res.json({ image: fichier.filename, alt: alt })
					})
				})
			}
		})
	}
})

app.post('/api/televerser-media', function (req, res) {
	const identifiant = req.session.identifiant
	if (!identifiant) {
		res.send('non_autorise')
	} else {
		televerser(req, res, function () {
			const fichier = req.file
			const info = path.parse(fichier.originalname)
			const alt = info.name
			const extension = info.ext.toLowerCase()
			const code = req.body.code
			const chemin = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + fichier.filename)
			if (extension === '.jpg' || extension === '.jpeg') {
				sharp(chemin).withMetadata().rotate().jpeg().resize(1000, 1000, {
					kernel: sharp.kernel.nearest,
					fit: 'inside'
				}).toBuffer((err, buffer) => {
					if (err) { res.send('erreur') }
					fs.writeFile(chemin, buffer, function() {
						res.json({ fichier: fichier.filename, alt: alt, type: 'image' })
					})
				})
			} else if (extension === '.png' || extension === '.gif') {
				sharp(chemin).withMetadata().resize(1000, 1000, {
					kernel: sharp.kernel.nearest,
					fit: 'inside'
				}).toBuffer((err, buffer) => {
					if (err) { res.send('erreur') }
					fs.writeFile(chemin, buffer, function() {
						res.json({ fichier: fichier.filename, alt: alt, type: 'image' })
					})
				})
			} else {
				res.json({ fichier: fichier.filename, alt: alt, type: 'audio' })
			}
		})
	}
})

app.post('/api/supprimer-fichiers', function (req, res) {
	const code = req.body.code
	const fichiers = req.body.fichiers
	fichiers.forEach(function (fichier) {
		supprimerFichier(code, fichier)
	})
	res.send('fichiers_supprimes')
})

app.post('/api/modifier-langue', function (req, res) {
	const langue = req.body.langue
	req.session.langue = langue
	res.send('langue_modifiee')
})

app.post('/api/modifier-nom', function (req, res) {
	const nom = req.body.nom
	req.session.nom = nom
	res.send('nom_modifie')
})

app.use(nuxt.render)

server.listen(port, host)

io.on('connection', function (socket) {
	socket.on('connexion', function (donnees) {
		const code = donnees.code
		const identifiant = donnees.identifiant
		const nom = donnees.nom
		socket.join(code)
		socket.room = code
		socket.identifiant = identifiant
		socket.nom = nom
		const clients = Object.keys(io.sockets.adapter.rooms[code].sockets)
		const utilisateurs = []
		for (let client of clients) {
			client = io.sockets.connected[client]
			utilisateurs.push({ identifiant: client.identifiant, nom: client.nom })
		}
		io.in(socket.room).emit('connexion', utilisateurs)
	})

	socket.on('deconnexion', function () {
		socket.to(socket.room).emit('deconnexion', socket.identifiant)
	})

	socket.on('interactionouverte', function (donnees) {
		socket.to(socket.room).emit('interactionouverte', donnees)
	})

	socket.on('interactionenattente', function (donnees) {
		socket.to(socket.room).emit('interactionenattente', donnees)
	})

	socket.on('interactionverrouillee', function () {
		socket.to(socket.room).emit('interactionverrouillee')
	})

	socket.on('interactiondeverrouillee', function () {
		socket.to(socket.room).emit('interactiondeverrouillee')
	})

	socket.on('interactionfermee', function () {
		socket.to(socket.room).emit('interactionfermee')
	})

	socket.on('questionsuivante', function (donnees) {
		socket.to(socket.room).emit('questionsuivante', donnees)
	})

	socket.on('classement', function (donnees) {
		socket.to(socket.room).emit('classement', donnees)
	})

	socket.on('nom', function (donnees) {
		socket.to(socket.room).emit('nom', donnees)
	})

	socket.on('reponse', function (reponse) {
		const code = parseInt(reponse.code)
		const session = parseInt(reponse.session)
		db.exists('interactions:' + code, function (err, donnees) {
			if (err) { socket.emit('erreur'); return false }
			if (donnees === 1) {
				db.hgetall('interactions:' + code, function (err, resultat) {
					if (err) { socket.emit('erreur'); return false }
					const type = resultat.type
					let reponses = JSON.parse(resultat.reponses)
					if (!reponses[session]) {
						reponses[session] = []
					}
					if (type !== 'Questionnaire') {
						reponses[session].push(reponse.donnees)
					} else {
						if (reponses[session].map(function (e) { return e.identifiant }).includes(reponse.donnees.identifiant) === true) {
							reponses[session].forEach(function (item) {
								if (item.identifiant === reponse.donnees.identifiant) {
									item.reponse = reponse.donnees.reponse
									item.score = reponse.donnees.score
								}
							})
						} else {
							reponses[session].push(reponse.donnees)
						}
					}
					db.hmset('interactions:' + code, 'reponses', JSON.stringify(reponses), function (err) {
						if (err) { socket.emit('erreur'); return false }
						socket.to(socket.room).emit('reponse', reponse)
						socket.emit('reponseenvoyee', reponse)
						socket.emit('reponses', { code: reponse.code, session: reponse.session, reponses: reponses[session] })
						socket.handshake.session.cookie.expires = new Date(Date.now() + (3600 * 24 * 7 * 1000))
						socket.handshake.session.save()
					})
				})
			} else {
				socket.emit('erreurcode'); return false
			}
		})
	})

	socket.on('supprimermessage', function (donnees) {
		const code = parseInt(donnees.code)
		const session = parseInt(donnees.session)
		const id = donnees.id
		db.exists('interactions:' + code, function (err, reponse) {
			if (err) { socket.emit('erreur'); return false }
			if (reponse === 1) {
				db.hgetall('interactions:' + code, function (err, resultat) {
					if (err) { socket.emit('erreur'); return false }
					let reponses = JSON.parse(resultat.reponses)
					if (reponses[session]) {
						reponses[session].forEach(function (item) {
							if (item.reponse.id === id) {
								item.reponse.visible = false
							}
						})
						db.hmset('interactions:' + code, 'reponses', JSON.stringify(reponses), function (err) {
							if (err) { socket.emit('erreur'); return false }
							io.in(socket.room).emit('reponses', { code: donnees.code, session: donnees.session, reponses: reponses[session] })
						})
					}
				})
			} else {
				socket.emit('erreurcode'); return false
			}
		})
	})

	socket.on('reorganisermessages', function (donnees) {
		const code = parseInt(donnees.code)
		const session = parseInt(donnees.session)
		db.exists('interactions:' + code, function (err, reponse) {
			if (err) { socket.emit('erreur'); return false }
			if (reponse === 1) {
				db.hgetall('interactions:' + code, function (err, resultat) {
					if (err) { socket.emit('erreur'); return false }
					let reponses = JSON.parse(resultat.reponses)
					if (reponses[session]) {
						reponses[session] = donnees.reponses
						db.hmset('interactions:' + code, 'reponses', JSON.stringify(reponses), function (err) {
							if (err) { socket.emit('erreur'); return false }
							io.in(socket.room).emit('reponses', { code: donnees.code, session: donnees.session, reponses: reponses[session] })
							socket.handshake.session.cookie.expires = new Date(Date.now() + (3600 * 24 * 7 * 1000))
							socket.handshake.session.save()
						})
					}
				})
			} else {
				socket.emit('erreurcode'); return false
			}
		})
	})
})

function creerMotDePasse () {
	let motdepasse = ''
	const lettres = 'abcdefghijklmnopqrstuvwxyz'
	for (let i = 0; i < 4; i++) {
		motdepasse += lettres.charAt(Math.floor(Math.random() * 26))
	}
	return motdepasse
}

function formaterDate (date, mot, langue) {
	let dateFormattee = ''
	switch (langue) {
		case 'fr':
			dateFormattee = mot + ' du ' + date
			break
		case 'en':
			dateFormattee = mot + ' of ' + date
			break
	}
	return dateFormattee
}

function definirStatistiquesSondage (items, reponses) {
	const total = reponses.length
	const personnes = []
	const pourcentages = []
	for (let i = 0; i < items.length; i++) {
		personnes.push(0)
		pourcentages.push(0)
	}
	items.forEach(function (item, index) {
		let nombreReponses = 0
		reponses.forEach(function (donnees) {
			donnees.reponse.forEach(function (reponse) {
				if (reponse === item.texte || reponse === item.image) {
					nombreReponses++
				}
			})
		})
		if (nombreReponses > 0) {
			personnes[index] = nombreReponses
			const pourcentage = (nombreReponses / total) * 100
			pourcentages[index] = Math.round(pourcentage)
		}
	})
	return { personnes: personnes, pourcentages: pourcentages }
}

function definirMessagesRemueMeninges (categories, reponses) {
	const messagesVisibles = []
	const messagesSupprimes = []
	for (let i = 0; i < categories.length; i++) {
		messagesVisibles.push([])
		messagesSupprimes.push([])
	}
	if (messagesVisibles.length > 0) {
		reponses.forEach(function (item) {
			let index = -1
			categories.forEach(function (categorie, indexCategorie) {
				if (item.reponse.categorie === categorie.texte || item.reponse.categorie === categorie.image) {
					index = indexCategorie
				}
			})
			if (item.reponse.visible && index > -1) {
				messagesVisibles[index].push(item)
			} else if (index > -1) {
				messagesSupprimes[index].push(item)
			}
		})
	} else {
		reponses.forEach(function (item) {
			if (item.reponse.visible) {
				messagesVisibles.push(item)
			} else {
				messagesSupprimes.push(item)
			}
		})
	}
	return { visibles: messagesVisibles, supprimes: messagesSupprimes }
}

function definirStatistiquesQuestionnaire (questions, reponses) {
	const statistiques = []
	questions.forEach(function (question, indexQuestion) {
		const personnes = []
		const pourcentages = []
		for (let i = 0; i < question.items.length; i++) {
			personnes.push(0)
			pourcentages.push(0)
		}
		question.items.forEach(function (item, index) {
			let total = 0
			let nombreReponses = 0
			reponses.forEach(function (donnees) {
				donnees.reponse[indexQuestion].forEach(function (reponse) {
					if (reponse === item.texte || reponse === item.image) {
						nombreReponses++
					}
				})
				total++
			})
			if (nombreReponses > 0) {
				personnes[index] = nombreReponses
				const pourcentage = (nombreReponses / total) * 100
				pourcentages[index] = Math.round(pourcentage)
			}
		}.bind(this))
		statistiques.push({ personnes: personnes, pourcentages: pourcentages })
	}.bind(this))
	return statistiques
}

const televerser = multer({
	storage: multer.diskStorage({
		destination: function (req, fichier, callback) {
			const code = req.body.code
			const chemin = path.join(__dirname, '..', '/static/fichiers/' + code + '/')
			callback(null, chemin)
		},
		filename: function (req, fichier, callback) {
			let extension = path.parse(fichier.originalname).ext
			extension = extension.toLowerCase()
			let nom = v.latinise(path.parse(fichier.originalname).name)
			nom = v.replaceAll(nom.toLowerCase(), ' ', '')
			nom = v.replaceAll(nom.toLowerCase(), '.', '') + '_' + Math.random().toString(36).substr(2) + extension
			callback(null, nom)
		}
	})
}).single('fichier')

function supprimerFichier (code, fichier) {
	const chemin = path.join(__dirname, '..', '/static/fichiers/' + code + '/' + fichier)
	fs.removeSync(chemin)
}
