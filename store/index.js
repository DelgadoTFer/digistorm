let hote = 'http://localhost:3000'
if (process.env.NODE_ENV === 'production') {
	hote = 'https://digistorm.app'
}

export const state = () => ({
	hote: hote,
	userAgent: '',
	message: '',
	notification: '',
	identifiant: '',
	nom: '',
	langue: 'fr'
})

export const mutations = {
	modifierUserAgent (state, donnees) {
		state.userAgent = donnees
	},
	modifierMessage (state, message) {
		state.message = message
	},
	modifierNotification (state, notification) {
		state.notification = notification
	},
	modifierIdentifiant (state, identifiant) {
		state.identifiant = identifiant
	},
	modifierNom (state, nom) {
		state.nom = nom
	},
	modifierLangue (state, langue) {
		state.langue = langue
	}
}

export const actions = {
	nuxtServerInit ({ commit }, { req }) {
		if (req.session && req.session.identifiant && req.session.identifiant !== '' && req.session.identifiant !== undefined) {
			commit('modifierIdentifiant', req.session.identifiant)
		}
		if (req.session && req.session.nom && req.session.nom !== '') {
			commit('modifierNom', req.session.nom)
		}
		if (req.session && req.session.langue && req.session.langue !== '') {
			commit('modifierLangue', req.session.langue)
		}
	},
	modifierUserAgent ({ commit }, userAgent) {
		commit('modifierUserAgent', userAgent)
	},
	modifierMessage ({ commit }, message) {
		commit('modifierMessage', message)
	},
	modifierNotification ({ commit }, notification) {
		commit('modifierNotification', notification)
	},
	modifierIdentifiant ({ commit }, identifiant) {
		commit('modifierIdentifiant', identifiant)
	},
	modifierNom ({ commit }, nom) {
		commit('modifierNom', nom)
	},
	modifierLangue ({ commit }, langue) {
		commit('modifierLangue', langue)
	}
}
